const secondHand = document.querySelector('.second-hand');
const minHand = document.querySelector('.min-hand');
const hourHand = document.querySelector('.hour-hand');


function setDate(){
    if(document.querySelector('.clock-page')){
        const now = new Date();
        const seconds = now.getSeconds();
        const min = now.getMinutes();
        const hour = now.getHours();
        const secondsDegree = ((seconds/60) * 360 ) + 90;
        const minDegree = ((min/60) * 360) + 90;
        const hourDegree = ((hour/12) * 360) + 90;
        secondHand.style.transform = `rotate(${secondsDegree}deg)`;
        minHand.style.transform = `rotate(${minDegree}deg)`;
        hourHand.style.transform = `rotate(${hourDegree}deg)`;
    }
    // console.log(new Date());
}

var variablesChanging = function(){
    const inputs = document.querySelectorAll('.controls input');
    function handleUpdate(){
       const suffix = this.dataset.sizing || "";
       document.documentElement.style.setProperty(`--${this.name}`,this.value + suffix);
    }

    inputs.forEach(input => input.addEventListener('change', handleUpdate))
}




const inventors = [
    { first: 'Albert', last: 'Einstein', year: 1879, passed: 1955 },
    { first: 'Isaac', last: 'Newton', year: 1643, passed: 1727 },
    { first: 'Galileo', last: 'Galilei', year: 1564, passed: 1642 },
    { first: 'Marie', last: 'Curie', year: 1867, passed: 1934 },
    { first: 'Johannes', last: 'Kepler', year: 1571, passed: 1630 },
    { first: 'Nicolaus', last: 'Copernicus', year: 1473, passed: 1543 },
    { first: 'Max', last: 'Planck', year: 1858, passed: 1947 },
    { first: 'Katherine', last: 'Blodgett', year: 1898, passed: 1979 },
    { first: 'Ada', last: 'Lovelace', year: 1815, passed: 1852 },
    { first: 'Sarah E.', last: 'Goode', year: 1855, passed: 1905 },
    { first: 'Lise', last: 'Meitner', year: 1878, passed: 1968 },
    { first: 'Hanna', last: 'Hammarström', year: 1829, passed: 1909 }
];

const people = ['Beck, Glenn', 'Becker, Carl', 'Beckett, Samuel', 'Beddoes, Mick', 'Beecher, Henry', 'Beethoven, Ludwig', 'Begin, Menachem', 'Belloc, Hilaire', 'Bellow, Saul', 'Benchley, Robert', 'Benenson, Peter', 'Ben-Gurion, David', 'Benjamin, Walter', 'Benn, Tony', 'Bennington, Chester', 'Benson, Leana', 'Bent, Silas', 'Bentsen, Lloyd', 'Berger, Ric', 'Bergman, Ingmar', 'Berio, Luciano', 'Berle, Milton', 'Berlin, Irving', 'Berne, Eric', 'Bernhard, Sandra', 'Berra, Yogi', 'Berry, Halle', 'Berry, Wendell', 'Bethea, Erin', 'Bevan, Aneurin', 'Bevel, Ken', 'Biden, Joseph', 'Bierce, Ambrose', 'Biko, Steve', 'Billings, Josh', 'Biondo, Frank', 'Birrell, Augustine', 'Black, Elk', 'Blair, Robert', 'Blair, Tony', 'Blake, William'];

  //Array.prototype.filter()
  //1. Filter the list of inventors those who were born in the 1500's
const fifteen = inventors.filter(function(inventor){
   if(inventor.year >=1500 && inventor.year < 1600){
      return true;
   }
});
console.log(fifteen);

 // Array.prototype.map()
    // 2. Give us an array of the inventor first and last names
    const fullNames = inventors.map(inventor => `${inventor.first} ${inventor.last}`);


    // Array.prototype.sort()
    // 3. Sort the inventors by birthdate, oldest to youngest
    // const ordered = inventors.sort(function(a, b) {
    //   if(a.year > b.year) {
    //     return 1;
    //   } else {
    //     return -1;
    //   }
    // });

    const ordered = inventors.sort((a, b) => a.year > b.year ? 1 : -1);


    // Array.prototype.reduce()
    // 4. How many years did all the inventors live?
    const totalYears = inventors.reduce((total, inventor) => {
      return total + (inventor.passed - inventor.year);
    }, 0);



    // 5. Sort the inventors by years lived
    const oldest = inventors.sort(function(a, b) {
      const lastInventor = a.passed - a.year;
      const nextInventor = b.passed - b.year;
      return lastInventor > nextInventor ? -1 : 1;
    });


    // 6. create a list of Boulevards in Paris that contain 'de' anywhere in the name
    // https://en.wikipedia.org/wiki/Category:Boulevards_in_Paris

    // const category = document.querySelector('.mw-category');
    // const links = Array.from(category.querySelectorAll('a'));
    // const de = links
    //             .map(link => link.textContent)
    //             .filter(streetName => streetName.includes('de'));

    // 7. sort Exercise
    // Sort the people alphabetically by last name
    const alpha = people.sort((lastOne, nextOne) => {
      const [aLast, aFirst] = lastOne.split(', ');
      const [bLast, bFirst] = nextOne.split(', ');
      return aLast > bLast ? 1 : -1;
    });


    // 8. Reduce Exercise
    // Sum up the instances of each of these
    const data = ['car', 'car', 'truck', 'truck', 'bike', 'walk', 'car', 'van', 'bike', 'walk', 'car', 'van', 'car', 'truck', 'pogostick'];

    const transportation = data.reduce(function(obj, item) {
      if (!obj[item]) {
        obj[item] = 0;
      }
      obj[item]++;
      return obj;
    }, {});



    const people2 = [
        { name: 'Wes', year: 1988 },
        { name: 'Kait', year: 1986 },
        { name: 'Irv', year: 1970 },
        { name: 'Lux', year: 2015 }
      ];

      const comments = [
        { text: 'Love this!', id: 523423 },
        { text: 'Super good', id: 823423 },
        { text: 'You are the best', id: 2039842 },
        { text: 'Ramen is my fav food ever', id: 123523 },
        { text: 'Nice Nice Nice!', id: 542328 }
      ];

      //Some and Every Cheks
      // Array.prototype.some()// is at least one person 19?
      // Arrey.prototype.every()// is everyone 19?
    //   const isAdult = people2.some(function(person){
    //      const currentYear = (new Data()).getFullYear();
    //      if(currentYear - person.year >= 19){
    //          return true;
    //      }
    //   })

    const isAdult = people2.some(person => ((new Date()).getFullYear()) - person.year >=19);
    console.log({isAdult});

    // Array .prototype.every () // is everyone 19?

    const allAdults = people.every(person => ((new Date()).getFullYear())- person.year >= 19);
    console.log(allAdults);


    // Array.prototype.find()
    // Find is like filter, but instead returns just the one you are looking for
    // find the comment with the ID of 823424

    // const comment = comments.find(function(comment){
    //   if(comment.id ===  823423){
    //     return true;
    //   }
    // });
    const comment = comments.find
    console.log(comment);

    // Array.prototype.findIndex();
    // Find the comment with this id
    // delete the comment with the ID of 823423

    const index = comments.findIndex(comment => comment.id === 823423);
    console.log(index);
    // comments.splice(index,1) - to delet item from the array with current index

    const newComments = [
      ...comments.slice(0,index),
      ...comments.slice(index+ 1)
    ];


    //--- References vs Copy ---

    // start with strings, numbers boolean

    // let age = 100;
    // let age2= age;
    // console.log(age2);
    // age = 200;
    // console.log(age2);

    // let name = 'News';
    // let name2 = name;
    // console.log(name2);
    // name = 'wes';
    // console.log(name2);

    // Lets says we have some array

    const players = ['wes', 'Sarah', 'Ryan', 'Poppy'];

    //and we want to make copy of it

    const team = players.slice();

    console.log(players, team);

    // You might think we can just do something like this:
    // team[3] = 'Lux';

    // however what happens when we update that array?

    // now here is the problem!

    // oh no - we have edited the original array too!

    // Why? It's because that is an array reference, not an array copy. They both point to the same array!

    // So, how do we fix this? We take a copy instead!
    const team2 = players.slice();

    // one day

    // or create a new array and concat the old one in
    const team3 = [].concat(players);

    // or use the new ES6 Spread
    const team4 = [...players];
    team4[3] = 'heeee hawww';
    console.log(team4);

    const team5 = Array.from(players);

    // now when we update it, the original one isn't changed

    // The same thing goes for objects, let's say we have a person object

    // with Objects

    const person = {
      name : 'Wes bos',
      age: 80
    }

    //and think we make a copy

    //const captain = person

    //captain.number == 99;

    // howdo we take a copy instead?

    const cap2 = Object.assign({},person,{number: 99});
    console.log(cap2, person);

    //we will hopefull soon see the object... spread

    // const cap3 = {...person};

    //things to note this is only 1 level deep - both for Arrays and Objects. Lodash has
    //a cloneDeep method, but you should think twice before
    //using it

    const wes={
      name: 'wes',
      age: 100,
      social: {
        twitter: '@wesbos',
        facebook: 'wesbos.develop'
      }
    }

    console.clear();
    console.log(wes);

    const dev = Object.assign({}, wes);
    const dev2 = JSON.parse(JSON.stringify(wes)); //deep copying of an element

    //webcam fan
    try {
      
  
      function getVideo(){
        const video = document.querySelector('.player');
        const canvas = document.querySelector('.photo');
        const ctx = canvas.getContext('2d');
        const strip = document.querySelector('.strip');
        const snap = document.querySelector('.snap');
        navigator.mediaDevices.getUserMedia({video: true, audio: false})
        .then(localMediaStream => {
          console.log(localMediaSteam);
          video.src = window.URL.createObjectURL(localMediaStream);
          video.play
        })
      }
    } catch (error) {
      console.log(error);
    }
    


    // const video = document.querySelector('.player');
    // const canvas = document.querySelector('.photo');
    // const ctx = canvas.getContext('2d');
    // const strip = document.querySelector('.strip');
    // const snap = document.querySelector('.snap');

function hilightningLinks(){
  if(document.querySelector('.highlight_links')){
    const globalWrapper = document.querySelector('.global-wrapper');
    const triggers = document.querySelectorAll('a');
    const highlight = document.createElement('span');

    highlight.classList.add('highlight');
    globalWrapper.append(highlight);

    function highlightLink(){
        const linkCoords = this.getBoundingClientRect();
        console.log(linkCoords);
        const coords = {
          width: linkCoords.width,
          height: linkCoords.height,
          top: linkCoords.top + window.scrollY,
          left: linkCoords.left + window.scrollX
        }
        highlight.style.width = `${coords.width}px`;
        highlight.style.height = `${coords.height}px`;
        highlight.style.transform = `translate(${coords.left}px, ${coords.top}px)`;
    }

    triggers.forEach(a => a.addEventListener('mouseenter', highlightLink));
  }

}


const msg = new SpeechSynthesisUtterance();

function voiceinator(){
  try{
    let voices = [];
    const voicesDropdown = document.querySelector('[name="voice"]');
    const options = document.querySelectorAll('[type="range"], [name="text"]');
    const speakButton = document.querySelector('#speak');
    const stopButton = document.querySelector('#stop');
    msg.text = document.querySelector('[name="text"]').value;
  
    function populateVoices(){
        voices = this.getVoices();
        console.log(voices);
        voicesDropdown.innerHTML = voices
            .map(voice => `<option value="${voice.name}">${voice.name}(${voice.lang})</option>`)
            .join('');
    }
  
    function setVoice() {
        msg.voice = voices.find(voice => voice.name === this.value);
        toggle();
    }
  
    function toggle(startOver = true){
      speechSynthesis.cancel();
      if(startOver){
        speechSynthesis.speak(msg);
      }
    }
  
    function setOption(){
      msg[this.name] = this.value;
      toggle();
    }
  
    speechSynthesis.addEventListener('voiceschanged', populateVoices);
    voicesDropdown.addEventListener('change', setVoice);
    options.forEach(option=> option.addEventListener('change', setOption));
    speakButton.addEventListener('click', toggle);
    stopButton.addEventListener('click', toggle.bind(null, false));
  } catch(err){
    console.log(err);
  }
}



function slidingHeader(){
  try{
    const nav = document.querySelector('#main');
    const topOfNav = nav.offsetTop;
    function fixNav(){
      if(window.scrollY >= topOfNav){
          document.body.style.paddingTop = nav.offsetHeight+'px';
          document.body.classList.add('fixed-nav');
      }else{
          document.body.style.padding = 0;
          document.body.classList.remove('fixed-nav');
      }
    }
    window.addEventListener('scroll', fixNav);
  } catch(err){
    console.log(err);
  }
  
}

function forDropdownNav(){
  const triggers = document.querySelectorAll('.cool > li');
  const background = document.querySelector('.dropdownBackground');
  const nav = document.querySelector('.top');

  function handleEnter(){
      this.classList.add('trigger-enter');
      setTimeout(() => {
        if(this.classList.contains('trigger-enter')){
          this.classList.add('trigger-enter-active');
        }
      },150);
      background.classList.add('open');
      const dropdown = this.querySelector('.dropdown');
      const dropdownCoords = dropdown.getBoundingClientRect();
      const navCoords = nav.getBoundingClientRect();
      const coords={
        height: dropdownCoords.height,
        width: dropdownCoords.width,
        left: dropdownCoords.left-navCoords.left,
        top: dropdownCoords.top - navCoords.top
      };
      background.style.setProperty('width', `${coords.width}px`);
      background.style.setProperty('height', `${coords.height}px`);
      background.style.setProperty('transform', `translate(${coords.left}px, ${coords.top}px)`);

  }

  function handleLeave(){
      this.classList.remove('trigger-enter', 'trigger-enter-active');
      background.classList.remove('open');
  }

  triggers.forEach(trigger => trigger.addEventListener('mouseenter', handleEnter));
  triggers.forEach(trigger => trigger.addEventListener('mouseleave', handleLeave));
}

function drug_and_scroll(){
  if(document.querySelector('.items')){
    const slider = document.querySelector('.items');
    let isDown = false;
    let startx;
    let scrollLeft;
  
    slider.addEventListener('mousedown', (e)=> {
      isDown = true;
      slider.classList.add('active');
      startx = e.pageX - slider.offsetLeft;
      scrollLeft = slider.scrollLeft;
      console.log(scrollLeft);
  
    })
  
    slider.addEventListener('mouseleave', ()=> {
      isDown = false;
      slider.classList.remove('active');
    })
  
    slider.addEventListener('mouseup', ()=> {
      isDown = false;
      slider.classList.remove('active');
    })
  
    slider.addEventListener('mousemove', (e)=> {
      if(!isDown) return; // stop the fn from runnnig
      e.preventDefault();
      const x = e.pageX - slider.offsetLeft;
      const walk = (x - startx)*3;
      slider.scrollLeft = scrollLeft - walk;
    })
  }  
}

try {
  let countdown;
  const timerDisplay = document.querySelector('.display__time-left');
  const endTime = document.querySelector('.display__end-time');
  const buttons = document.querySelectorAll('[data-time]');


  function timer(seconds){
    //clear any existing timer
    clearInterval(countdown);

    const now = Date.now();
    const then = now + seconds * 1000;
    displayTimeLeft(seconds);
    displayEndTime(then);
    countdown = setInterval(()=>{
      const secondsLeft = Math.round((then - Date.now())/ 1000);
      //check if we should stop it!
      if(secondsLeft<= 0){
        clearInterval(countdown);
        return;
      }
      //display it
      displayTimeLeft(secondsLeft);
    },1000);
  };

  function displayTimeLeft(seconds){
    const minutes = Math.floor(seconds/60);
    const remainderSeconds = seconds % 60;
    const display = `${minutes}:${remainderSeconds <10 ? '0':'' }${remainderSeconds}`;
    document.tittle = display;
    timerDisplay.textContent = display;
  };

  function displayEndTime(timestamp){
    const end = new Date(timestamp);
    const hour = end.getHours();
    const minutes = end.getMinutes();
    endTime.textContent = `Be back at ${hour}:${minutes <10 ? '0':'' }${minutes}`;
  }

  function startTimer(){
    const seconds = parseInt(this.dataset.time);
    timer(seconds);
  }

  buttons.forEach(button => {
      button.addEventListener('click', startTimer);
  });

  document.customForm.addEventListener('submit', function(e){
    e.preventDefault();
    const mins = this.minutes.value;
    timer(mins*60);
    this.reset();
  })

} catch (err) {

  console.log(err)

}


//mole game
// function mole_game(){
  try {
    const holes = document.querySelectorAll('.hole');
    const scoreBoard = document.querySelector('.score');
    const moles = document.querySelectorAll('.mole');
    const start_button = document.querySelector('.start-button');
    let lastHole;
    let timeUp=false;
    let score = 0;
  
    function randTime(min, max){
      return Math.round(Math.random()*(max - min) + min);
    }
  
    function randomHole(holes){
      const idx = Math.floor(Math.random() * holes.length);
      const hole = holes[idx];
      if(hole === lastHole){
        return randomHole(holes);
      }
      lastHole = hole;
      return hole;
    }
  
    function peep(){
      const time = randTime(200, 1000);
      const hole = randomHole(holes);
      hole.classList.add('up');
      setTimeout(()=>{
        hole.classList.remove('up');
        if(!timeUp) peep();
      },time);
    }
  
    function startGame(){
      scoreBoard.textContent = 0;
      score = 0 ;
      timeUp = false;
      peep();
      setTimeout(()=> timeUp = true, 10000 )
    }
  
    function bonk(e){
      if(!e.isTrusted && this.parentElement.classList.contains('up') ) return;
      score++;
      this.parentElement.classList.remove('up');
      scoreBoard.textContent = score;
    }
  
    moles.forEach(mole => {
      mole.addEventListener('click', bonk);
    });
  
    start_button.addEventListener('click', startGame);
  } catch (error) {
    console.log(error);
  }
  

// }

$(document).ready(function(){
    setInterval(setDate, 1000);
    variablesChanging();
    hilightningLinks();
    voiceinator();
    slidingHeader();
    forDropdownNav();
    drug_and_scroll();
    // mole_game();
});

$(window).load(function(){

});

$(window).resize(function(){

});
